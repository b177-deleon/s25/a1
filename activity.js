db.fruits.aggregate([
	   { $match: { onSale: true } },
	   { $count: "name" }
]);

db.fruits.aggregate([
	   { $match: { stock: { $gt: 20 } } },
	   { $count: "name" }
]);

db.fruits.aggregate([
   { $match: { onSale: true } },
   { $group: { _id: "$supplier_id", averagePrice: { $avg: "$price" } } }
]);

db.fruits.aggregate([
   { $group: { _id: "$supplier_id", highestPrice: { $max: "$price" } } }
]);

db.fruits.aggregate([
   { $group: { _id: "$supplier_id", highestPrice: { $min: "$price" } } }
]);